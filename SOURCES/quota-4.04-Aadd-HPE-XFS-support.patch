From a92dcf5a6cc49660d75a67966b0eb093b88e3b4c Mon Sep 17 00:00:00 2001
From: "Tinguely, Mark" <mark.tinguely@hpe.com>
Date: Wed, 9 May 2018 16:50:25 +0200
Subject: [PATCH] Aadd HPE XFS support
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

HPE XFS has a different superblock magic and type name
to allow the community XFS and HPE XFS to coexist in
the same linux kernel.

This patch add HPE XFS support to the quota-tools so
our customers can use it.

Signed-off-by: Mark Tinguely <mark.tinguely@hpe.com>
Signed-off-by: Jan Kara <jack@suse.cz>
Signed-off-by: Petr Písař <ppisar@redhat.com>
---
 bylabel.c    |  4 +++-
 mntopt.h     |  1 +
 quot.c       |  3 ++-
 quotacheck.c |  5 +++--
 quotaio.c    |  3 ++-
 quotaon.c    |  6 ++++--
 quotasys.c   | 10 +++++++---
 7 files changed, 22 insertions(+), 10 deletions(-)

diff --git a/bylabel.c b/bylabel.c
index ff10422..5313461 100644
--- a/bylabel.c
+++ b/bylabel.c
@@ -48,6 +48,7 @@ struct ext2_super_block {
 
 #define XFS_SUPER_MAGIC "XFSB"
 #define XFS_SUPER_MAGIC2 "BSFX"
+#define EXFS_SUPER_MAGIC "EXFS"
 struct xfs_super_block {
 	u_char s_magic[4];
 	u_char s_dummy[28];
@@ -107,7 +108,8 @@ static int get_label_uuid(const char *device, char **label, char *uuid)
 	else if (lseek(fd, 0, SEEK_SET) == 0
 		 && read(fd, (char *)&xfsb, sizeof(xfsb)) == sizeof(xfsb)
 		 && (strncmp((char *)&xfsb.s_magic, XFS_SUPER_MAGIC, 4) == 0 ||
-		     strncmp((char *)&xfsb.s_magic, XFS_SUPER_MAGIC2, 4) == 0)) {
+		     strncmp((char *)&xfsb.s_magic, XFS_SUPER_MAGIC2, 4) == 0 ||
+		     strncmp((char *)&xfsb.s_magic, EXFS_SUPER_MAGIC, 4) == 0)) {
 		memcpy(uuid, xfsb.s_uuid, sizeof(xfsb.s_uuid));
 		namesize = sizeof(xfsb.s_fsname);
 		*label = smalloc(namesize + 1);
diff --git a/mntopt.h b/mntopt.h
index 7913048..0f3b0c5 100644
--- a/mntopt.h
+++ b/mntopt.h
@@ -15,6 +15,7 @@
 #define MNTTYPE_UDF		"udf"	/* OSTA UDF file system */
 #define MNTTYPE_REISER		"reiserfs"	/* Reiser file system */
 #define MNTTYPE_XFS		"xfs"	/* SGI XFS file system */
+#define MNTTYPE_EXFS		"exfs"	/* HPE EXFS file system */
 #define MNTTYPE_AUTOFS		"autofs"	/* Automount mountpoint */
 #define MNTTYPE_JFS		"jfs"	/* JFS file system */
 #define MNTTYPE_NFS4		"nfs4"	/* NFSv4 filesystem */
diff --git a/quot.c b/quot.c
index 6311f77..c3a31a4 100644
--- a/quot.c
+++ b/quot.c
@@ -143,7 +143,8 @@ static void mounttable(void)
 
 	while ((mntp = get_next_mount())) {
 		/* Currently, only XFS is implemented... */
-		if (strcmp(mntp->me_type, MNTTYPE_XFS) == 0) {
+		if (strcmp(mntp->me_type, MNTTYPE_XFS) == 0 ||
+		    strcmp(mntp->me_type, MNTTYPE_EXFS) == 0) {
 			checkXFS(mntp->me_devname, mntp->me_dir);
 			doit = 1;
 		}
diff --git a/quotacheck.c b/quotacheck.c
index b1302b6..fd01dfc 100644
--- a/quotacheck.c
+++ b/quotacheck.c
@@ -1042,7 +1042,8 @@ static int detect_filename_format(struct mount_entry *mnt, int type)
 	int fmt;
 
 	if (strcmp(mnt->me_type, MNTTYPE_XFS) == 0 ||
-	    strcmp(mnt->me_type, MNTTYPE_GFS2) == 0)
+	    strcmp(mnt->me_type, MNTTYPE_GFS2) == 0 ||
+	    strcmp(mnt->me_type, MNTTYPE_EXFS) == 0)
 		return QF_XFS;
 
 	if (type == USRQUOTA) {
@@ -1116,7 +1117,7 @@ static int compatible_fs_qfmt(char *fstype, int fmt)
 {
 	/* We never check XFS, NFS, and filesystems supporting VFS metaformat */
 	if (!strcmp(fstype, MNTTYPE_XFS) || nfs_fstype(fstype) ||
-	    meta_qf_fstype(fstype))
+	    meta_qf_fstype(fstype) || !strcmp(fstype, MNTTYPE_EXFS))
 		return 0;
 	/* In all other cases we can pick a format... */
 	if (fmt == -1)
diff --git a/quotaio.c b/quotaio.c
index df893c6..94ae458 100644
--- a/quotaio.c
+++ b/quotaio.c
@@ -79,7 +79,8 @@ struct quota_handle *init_io(struct mount_entry *mnt, int type, int fmt, int fla
 	}
 
 	if (!strcmp(mnt->me_type, MNTTYPE_XFS) ||	/* XFS filesystem? */
-	    !strcmp(mnt->me_type, MNTTYPE_GFS2)) {	/* XFS filesystem? */
+	    !strcmp(mnt->me_type, MNTTYPE_GFS2) ||	/* XFS filesystem? */
+	    !strcmp(mnt->me_type, MNTTYPE_EXFS)) {	/* EXFS filesystem? */
 		if (fmt != -1 && fmt != QF_XFS) {	/* User wanted some other format? */
 			errstr(_("Only XFS quota format is allowed on XFS filesystem.\n"));
 			goto out_handle;
diff --git a/quotaon.c b/quotaon.c
index fe19224..cee2e25 100644
--- a/quotaon.c
+++ b/quotaon.c
@@ -282,7 +282,8 @@ static int newstate(struct mount_entry *mnt, int type, char *extra)
 	if (!strcmp(mnt->me_type, MNTTYPE_GFS2)) {
 		errstr(_("Cannot change state of GFS2 quota.\n"));
 		return 1;
-	} else if (!strcmp(mnt->me_type, MNTTYPE_XFS)) {	/* XFS filesystem has special handling... */
+	} else if (!strcmp(mnt->me_type, MNTTYPE_XFS) ||
+		   !strcmp(mnt->me_type, MNTTYPE_EXFS)) {	/* XFS filesystem has special handling... */
 		if (!kern_qfmt_supp(QF_XFS)) {
 			errstr(_("Cannot change state of XFS quota. It's not compiled in kernel.\n"));
 			return 1;
@@ -337,7 +338,8 @@ static int print_state(struct mount_entry *mnt, int type)
 	if (kern_qfmt_supp(QF_XFS)) {
 		on = kern_quota_state_xfs(mnt->me_devname, type);
 		if (!strcmp(mnt->me_type, MNTTYPE_XFS) ||
-		    !strcmp(mnt->me_type, MNTTYPE_GFS2) || on >= 0) {
+		    !strcmp(mnt->me_type, MNTTYPE_GFS2) || on >= 0 ||
+		    !strcmp(mnt->me_type, MNTTYPE_EXFS)) {
 			if (on < 0)
 				on = 0;
 			if (!(flags & FL_VERBOSE))
diff --git a/quotasys.c b/quotasys.c
index 9336411..d8c0e48 100644
--- a/quotasys.c
+++ b/quotasys.c
@@ -781,7 +781,8 @@ static void copy_mntoptarg(char *buf, const char *optarg, int buflen)
 static int hasquota(const char *dev, struct mntent *mnt, int type, int flags)
 {
 	if (!strcmp(mnt->mnt_type, MNTTYPE_GFS2) ||
-	    !strcmp(mnt->mnt_type, MNTTYPE_XFS))
+	    !strcmp(mnt->mnt_type, MNTTYPE_XFS) ||
+	    !strcmp(mnt->mnt_type, MNTTYPE_EXFS))
 		return hasxfsquota(dev, mnt, type, flags);
 	if (!strcmp(mnt->mnt_type, MNTTYPE_OCFS2))
 		return hasvfsmetaquota(dev, mnt, type, flags);
@@ -953,12 +954,14 @@ add_entry:
 				break;
 			case QF_XFS:
 				if (!strcmp(mnt->me_type, MNTTYPE_XFS) ||
-				    !strcmp(mnt->me_type, MNTTYPE_GFS2))
+				    !strcmp(mnt->me_type, MNTTYPE_GFS2) ||
+				    !strcmp(mnt->me_type, MNTTYPE_EXFS))
 					goto add_entry;
 				break;
 			default:
 				if (strcmp(mnt->me_type, MNTTYPE_XFS) &&
 				    strcmp(mnt->me_type, MNTTYPE_GFS2) &&
+				    strcmp(mnt->me_type, MNTTYPE_EXFS) &&
 				    !nfs_fstype(mnt->me_type))
 					goto add_entry;
 				break;
@@ -1059,7 +1062,8 @@ void init_kernel_interface(void)
 	else {
 		struct v2_dqstats v2_stats;
 
-		if (!stat("/proc/fs/xfs/stat", &st))
+		if (!stat("/proc/fs/xfs/stat", &st) ||
+		    !stat("/proc/fs/exfs/stat", &st))
 			kernel_qfmt[kernel_qfmt_num++] = QF_XFS;
 		else {
 			fs_quota_stat_t dummy;
-- 
2.26.3

